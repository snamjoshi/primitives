{
    "id": "dsbox-featurizer-object-detection-yolo",
    "version": "1.5.0",
    "name": "DSBox Object Detection YOLO",
    "description": "Object detection primitive that use YOLOv3 algorithm\noffical site available at : https://pjreddie.com/darknet/yolo/\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.\n\nParameters\n----------\ntarget_class_id: The id of the object detection targets\noutput_layer: The list of the output layer number from YOLO's DNN",
    "python_path": "d3m.primitives.feature_extraction.yolo.DSBOX",
    "primitive_family": "FEATURE_EXTRACTION",
    "algorithm_types": [
        "DEEP_NEURAL_NETWORK"
    ],
    "keywords": [
        "image",
        "featurization",
        "yolo"
    ],
    "source": {
        "name": "ISI",
        "contact": "mailto:kyao@isi.edu",
        "uris": [
            "https://github.com/usc-isi-i2/dsbox-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/usc-isi-i2/dsbox-primitives@93674fdf69ae9659fc44dd04ea3d7277f7966d82#egg=dsbox-primitives"
        }
    ],
    "precondition": [],
    "hyperparms_to_tune": [],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "dsbox.datapreprocessing.featurizer.image.object_detection.Yolo",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "dsbox.datapreprocessing.featurizer.image.object_detection.Params",
            "Hyperparams": "dsbox.datapreprocessing.featurizer.image.object_detection.YoloHyperparams"
        },
        "interfaces_version": "2019.4.4",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "use_fitted_weight": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A control parameter to set whether to use the pre-trained model weights or train new model"
            },
            "output_to_tmp_dir": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to output the images with bounding boxes for debugging purpose"
            },
            "blob_scale_factor": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.00392,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "multiplier for image values for cv.dnn.blobFromImage function",
                "lower": 0,
                "upper": 1,
                "upper_inclusive": false
            },
            "blob_output_shape_x": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 416,
                "structural_type": "int",
                "semantic_types": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": " spatial size for output image (x-dimension) in blob",
                "lower": 0,
                "upper": 9223372036854775807,
                "upper_inclusive": false
            },
            "blob_output_shape_y": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 416,
                "structural_type": "int",
                "semantic_types": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": " spatial size for output image (y-dimension) in blob",
                "lower": 0,
                "upper": 9223372036854775807,
                "upper_inclusive": false
            },
            "blob_mean_R": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "scalar with mean values which are subtracted from channels - color R",
                "lower": 0,
                "upper": 255,
                "upper_inclusive": false
            },
            "blob_mean_G": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "scalar with mean values which are subtracted from channels - color G",
                "lower": 0,
                "upper": 255,
                "upper_inclusive": false
            },
            "blob_mean_B": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "scalar with mean values which are subtracted from channels - color B",
                "lower": 0,
                "upper": 255,
                "upper_inclusive": false
            },
            "blob_crop": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "flag which indicates whether image will be cropped after resize or not"
            },
            "confidences_threshold": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.5,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "threshold of the confident to use the predictions",
                "lower": 0,
                "upper": 1,
                "upper_inclusive": false
            },
            "nms_threshold": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.4,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "threshold of the non-max suppression",
                "lower": 0,
                "upper": 1,
                "upper_inclusive": false
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "dsbox.datapreprocessing.featurizer.image.object_detection.YoloHyperparams",
                "kind": "RUNTIME"
            },
            "volumes": {
                "type": "typing.Union[NoneType, typing.Dict[str, str]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "dsbox.datapreprocessing.featurizer.image.object_detection.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "dsbox.datapreprocessing.featurizer.image.object_detection.YoloHyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "volumes"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "If using the pre-training model, here we will use this model to detect what inside the bounding boxes from\ntraining dataset. Then, count the number of the objects detected in each box, we will treat only the objects\namount number larger than the threshold to be the target that we need to detect in the test part.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "dsbox.datapreprocessing.featurizer.image.object_detection.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Use YOLO to detect the objects in the input dataframe\nThe function will read the images if the input is a dataframe with image names\nThe detected output depends on the input training dataset's ojbects\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\noutputs : Outputs\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "target_class_id": "typing.List[int]",
            "output_layer": "typing.List[str]",
            "target_column_name": "str"
        }
    },
    "structural_type": "dsbox.datapreprocessing.featurizer.image.object_detection.Yolo",
    "digest": "ce97b1064ae308dd1d9853aad298e7052d6e23ffefe2022e68c3b188f3619111"
}
